import QtQuick 2.0
import GameObject 1.0
import GameEngine 1.0

Item {
    property string black: "#D18B47"
    property string white: "#FFCE9E"
    property int cellWidth: 80
    property int cellHeight: 80
    property var arrLetters: ["A", "B", "C", "D", "E", "F", "G", "H"]
    property var arrDigits: ["8", "7", "6", "5", "4", "3", "2", "1"]
    property int pos: 0

    id: chessBoard
    objectName: "chess-board"
    anchors.top: parent.top
    anchors.left: parent.left
    anchors.topMargin: 50
    anchors.leftMargin: 100

    ObjectList {
        id: objects
    }

    Row {
        anchors.bottom: grid.top
        anchors.bottomMargin: 1

        Repeater {
            model: arrLetters
            delegate: Rectangle {
                width: cellWidth
                height: 30
                color: white

                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: black
                    text: modelData
                    font.bold: true
                    font.pixelSize: 16
                }
            }
        }
    }

    Column {
        anchors.right: grid.left
        anchors.rightMargin: 1

        Repeater {
            model: arrDigits
            delegate: Rectangle {
                height: cellHeight
                width: 30
                color: white

                Text {
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: modelData
                    color: black
                    font.bold: true
                    font.pixelSize: 16
                }
            }
        }
    }

    Grid {
        id: grid
        width: 640
        height: 640
        columns: 8
        rows: 8

        Repeater {
            id: cells
            model: 64

            Rectangle {
                id: rect
                width: cellWidth
                height: cellHeight
                color: (Math.floor(index / 8) + (index % 8)) % 2 ? black : white

                MouseArea {
                    anchors.fill: parent
                    onClicked:
                    {
                        pieces.itemAt(pos).x = rect.x
                        pieces.itemAt(pos).y = rect.y
                    }
                }
            }
        }

        Repeater {
            id: pieces
            model: objects.gameObjects

            Item {
                id: piece
                width: cellWidth
                height: cellHeight
                x: model.modelData.pieceX
                y: model.modelData.pieceY

                Rectangle {
                    id: lol
                    anchors.fill: parent
                    color: "#80FFFF00"  // ARGB
                    visible: false
                }

                ChessPiece {
                    anchors.fill: parent
                    type: model.modelData.type
                    selected: model.modelData.selected
                    position: model.modelData.position
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked:
                    {
                        model.modelData.selected = !model.modelData.selected
                        lol.visible = model.modelData.selected
                        pos = index
                    }
                }
            }
        }
    }

    ListView {
        anchors.left: grid.right
        anchors.leftMargin: 50
        width: cellWidth
        height: cellHeight * 5

        model: objects.gameObjects
        delegate: Item {
            width: cellWidth
            height: cellHeight

            ChessPiece {
                anchors.fill: parent
                type: model.modelData.type
                selected: model.modelData.selected
                position: model.modelData.position
            }
        }
    }
}
