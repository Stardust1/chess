import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item
{
    property string buttonText: "text"

    Button
    {
        text: buttonText

        style: ButtonStyle
        {
            background: Rectangle
            {
                implicitWidth: 200
                implicitHeight: 75
                border.width: control.activeFocus ? 2 : 1
                border.color: "#888888"
                radius: 4
                gradient: Gradient
                {
                    GradientStop
                    {
                        position: 0
                        color: control.pressed ? "#CCCCCC" : "#EEEEEE"
                    }
                    GradientStop
                    {
                        position: 1
                        color: control.pressed ? "#AAAAAA" : "#CCCCCC"
                    }
                }
            }
        }
    }
}
