import QtQuick 2.0


Item
{
    width: 300
    height: 500

    MenuButton
    {
        buttonText: "Resume"
        anchors.top: parent.top
        anchors.margins: 0
    }

    MenuButton
    {
        buttonText: "New Game"
        anchors.top: parent.top
        anchors.margins: 80
    }

    MenuButton
    {
        buttonText: "Save Game"
        anchors.top: parent.top
        anchors.margins: 160
    }

    MenuButton
    {
        buttonText: "Load Game"
        anchors.top: parent.top
        anchors.margins: 240
    }

    MenuButton
    {
        buttonText: "Exit"
        anchors.top: parent.top
        anchors.margins: 320
    }
}
