#include "gameengine.h"

GameEngine::GameEngine()
{
    start();
}

QList<QObject *> GameEngine::gameObjects() const
{
    return m_objects;
}

void GameEngine::setGameObjects(QList<QObject *> list)
{
    if(m_objects != list)
    {
        m_objects = list;
        gameObjectsChanged(m_objects);
    }
}

void GameEngine::start()
{
    QList<QObject *> objectList;

    objectList.append(new GameObject(0, 0,   0, GameObject::Type::BlackRook));
    objectList.append(new GameObject(1, 80,  0, GameObject::Type::BlackKnight));
    objectList.append(new GameObject(2, 160, 0, GameObject::Type::BlackBishop));
    objectList.append(new GameObject(3, 240, 0, GameObject::Type::BlackQueen));
    objectList.append(new GameObject(4, 320, 0, GameObject::Type::BlackKing));
    objectList.append(new GameObject(5, 400, 0, GameObject::Type::BlackBishop));
    objectList.append(new GameObject(6, 480, 0, GameObject::Type::BlackKnight));
    objectList.append(new GameObject(7, 560, 0, GameObject::Type::BlackRook));

    int posX = 0;
    for(int pos = 8; pos != 16; ++pos)
    {
        objectList.append(new GameObject(pos, posX, 80, GameObject::Type::BlackPawn));
        posX += 80;
    }

    setGameObjects(objectList);
}
