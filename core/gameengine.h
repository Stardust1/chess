#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <QObject>
#include "gameobject.h"


class GameEngine: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QList<QObject *> gameObjects READ gameObjects WRITE setGameObjects NOTIFY gameObjectsChanged)

public:
    GameEngine();

    QList<QObject *> gameObjects() const;
    void setGameObjects(QList<QObject *> list);

    void start();

signals:
    void gameObjectsChanged(QList<QObject *> list);

private:
    QList<QObject *> m_objects;
};

#endif // GAMEENGINE_H
