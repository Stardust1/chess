#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <QQuickPaintedItem>
#include <QImage>
#include "gameobject.h"


class GameObject: public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(int position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(int pieceX READ pieceX WRITE setPieceX NOTIFY pieceXChanged)
    Q_PROPERTY(int pieceY READ pieceY WRITE setPieceY NOTIFY pieceYChanged)
    Q_PROPERTY(Type type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged)

public:
    explicit GameObject(QQuickPaintedItem *parent = nullptr);

    enum Type {WhitePawn, WhiteBishop, WhiteKnight, WhiteRook, WhiteQueen, WhiteKing, BlackPawn, BlackBishop, BlackKnight, BlackRook, BlackQueen, BlackKing};
    Q_ENUM(Type)
    GameObject(int position, int posX, int posY, Type type, QQuickPaintedItem *parent = nullptr);


    void paint(QPainter *p);

    int position();
    void setPosition(int newPosition);

    int pieceX();
    void setPieceX(int newPieceX);

    int pieceY();
    void setPieceY(int newPieceY);

    Type type();
    void setType(Type newType);

    bool selected();
    void setSelected(bool newSelect);

signals:
    void pieceXChanged(int newPieceX);
    void pieceYChanged(int newPieceY);
    void positionChanged(int newPosition);
    void typeChanged(int newType);
    void selectedChanged(bool newSelect);

private:
    int m_position;
    int m_pieceX;
    int m_pieceY;
    Type m_type;
    bool m_selected;

    QImage m_texture;
};

#endif // GAMEOBJECT_H
