#include "gameobject.h"
#include <QPainter>
#include <QDir>
#include <QDebug>


namespace Internal
{
    static const QMap<int, QString> textures
    {
        { GameObject::Type::WhitePawn,   QString(QDir::currentPath() + "/images/pieces/white_pawn.png")   },
        { GameObject::Type::WhiteBishop, QString(QDir::currentPath() + "/images/pieces/white_bishop.png") },
        { GameObject::Type::WhiteKnight, QString(QDir::currentPath() + "/images/pieces/white_knight.png") },
        { GameObject::Type::WhiteRook,   QString(QDir::currentPath() + "/images/pieces/white_rook.png")   },
        { GameObject::Type::WhiteQueen,  QString(QDir::currentPath() + "/images/pieces/white_queen.png")  },
        { GameObject::Type::WhiteKing,   QString(QDir::currentPath() + "/images/pieces/white_king.png")   },
        { GameObject::Type::BlackPawn,   QString(QDir::currentPath() + "/images/pieces/black_pawn.png")   },
        { GameObject::Type::BlackBishop, QString(QDir::currentPath() + "/images/pieces/black_bishop.png") },
        { GameObject::Type::BlackKnight, QString(QDir::currentPath() + "/images/pieces/black_knight.png") },
        { GameObject::Type::BlackRook,   QString(QDir::currentPath() + "/images/pieces/black_rook.png")   },
        { GameObject::Type::BlackQueen,  QString(QDir::currentPath() + "/images/pieces/black_queen.png")  },
        { GameObject::Type::BlackKing,   QString(QDir::currentPath() + "/images/pieces/black_king.png")   }
    };
}

GameObject::GameObject(QQuickPaintedItem *parent)
    : QQuickPaintedItem(parent)
{
    setSelected(false);
    setPieceX(0);
    setPieceY(0);
    setPosition(0);
    setType(Type::WhiteQueen);
}


GameObject::GameObject(int position, int posX, int posY, GameObject::Type type, QQuickPaintedItem *parent)
    : QQuickPaintedItem(parent)
{
    setSelected(false);
    setPosition(position);
    setPieceX(posX);
    setPieceY(posY);
    setType(type);
}

void GameObject::paint(QPainter *p)
{
     p->drawImage(boundingRect(), m_texture);
}

int GameObject::position()
{
    return m_position;
}

void GameObject::setPosition(int newPosition)
{
    if(newPosition != m_position)
    {
        m_position = newPosition;
        positionChanged(m_position);
    }
}

int GameObject::pieceX()
{
    return m_pieceX;
}

void GameObject::setPieceX(int newPieceX)
{
    if(newPieceX != m_pieceX)
    {
        m_pieceX = newPieceX;
        pieceXChanged(m_pieceX);
    }
}

int GameObject::pieceY()
{
    return m_pieceY;
}

void GameObject::setPieceY(int newPieceY)
{
    if(newPieceY != m_pieceY)
    {
        m_pieceY = newPieceY;
        pieceYChanged(m_pieceY);
    }
}

GameObject::Type GameObject::type()
{
    return m_type;
}

void GameObject::setType(GameObject::Type newType)
{
    if(newType != m_type)
    {
        m_type = newType;
        typeChanged(m_type);
    }
    m_texture = QImage(Internal::textures[m_type]);
}

bool GameObject::selected()
{
    return m_selected;
}

void GameObject::setSelected(bool newSelect)
{
    //qDebug() << newSelect;
    if(newSelect != m_selected)
    {
        m_selected = newSelect;
        selectedChanged(m_selected);
    }
}
