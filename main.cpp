#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include "gameobject.h"
#include "gameengine.h"


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<GameObject>("GameObject", 1, 0, "ChessPiece");
    qmlRegisterType<GameEngine>("GameEngine", 1, 0, "ObjectList");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));


    return app.exec();
}
