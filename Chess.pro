TEMPLATE = app

QT += qml quick

CONFIG += c++11

HEADERS += \
    core/applicationmanager.h \
    core/gameengine.h \
    core/gameobject.h

SOURCES += main.cpp \
    core/applicationmanager.cpp \
    core/gameengine.cpp \
    core/gameobject.cpp

RESOURCES += chess.qrc

INCLUDEPATH += core

OTHER_FILES += main.qml \
    qml/Menu.qml \
    qml/Game.qml \
    qml/MenuButton.qml \
    images/pieces/*
