import QtQuick 2.5
import QtQuick.Window 2.2
import GameObject 1.0

import "qrc:/qml"

Window {
    visible: true
    width: 1200
    height: 800
    title: qsTr("Hello World")

//    Menu
//    {
//        anchors.top: parent.top
//        anchors.left: parent.left
//        anchors.topMargin: 50
//        anchors.leftMargin: 200
//    }

    Rectangle
    {
        anchors.fill: parent
        color: "#696969"
    }

    Game {
        width: 1000
        height: 700
    }
}
